;;; indiumeval-parse-elisp.el --- Get data from elisp code -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;
;; Functions for parsing and getting data from elisp code.
;;

;;; Code:

;; (indiumeval-parse-elisp--arg-index-at :: Number -> Number -> Number)
(defun indiumeval-parse-elisp--arg-index-at (point-of-caller-func
                                             point-inside-argument)
  "Return argument index of caller func.
POINT-OF-CALLER-FUNC is the point where the caller function is located.
POINT-INSIDE-ARGUMENT is any point inside argument."
  (save-excursion
    (goto-char point-of-caller-func)
    (backward-char)
    (let ((index 0))
      (while (<= (point) point-inside-argument)
        (forward-sexp)
        (setq index (1+ index)))
      (1- index))))

;; (indiumeval-parse-elisp--jump-to-calling-sexp-maybe :: Nil -> Number)
(defun indiumeval-parse-elisp--jump-to-calling-sexp-maybe ()
  "If success, return point of sexp, else nil."
  (condition-case nil
      (progn
        ;; `in-string-p' is obsolete since 25.1;
        ;; use (nth 3 (syntax-ppss)) instead.
        (if (nth 3 (syntax-ppss))
            (up-list 2 t)
          (up-list))
        (backward-sexp)
        (point))
    ((error) nil)))

;; (indiumeval-parse-elisp--containing-sexp-point :: [String] -> Number?)
(defun indiumeval-parse-elisp--containing-sexp-point (allowed-func-names)
  "Return either nil or point of outer sexp if found.
Point should be inside a string argument of one function included in ALLOWED-FUNC-NAMES."
  (save-excursion
    (when (and (indiumeval-parse-elisp--jump-to-calling-sexp-maybe)
               ;; evals to t if any name in allowed-fun-names matches
               (eval
                (cons 'or
                      (mapcar (lambda (fun-name)
                                (looking-at (concat "(" fun-name)))
                              allowed-func-names))))
      (point))))

;; (indiumeval-parse-elisp--calling-func-region :: [String] -> [Mixed])
(defun indiumeval-parse-elisp--calling-func-region (allowed-func-names)
  "If found, return name and region for outer calling function.
ALLOWED-FUNC-NAMES is a list containing allowed calling function names."
  (let ((p (indiumeval-parse-elisp--containing-sexp-point
            allowed-func-names))
        reg-beg
        reg-end)
    (when p
      (save-excursion
        (goto-char p)
        (down-list)
        (forward-sexp)
        (skip-chars-forward " \n")
        (setq reg-beg (1+ (point)))
        (forward-sexp)
        (setq reg-end (1- (point)))
        `(,reg-beg . ,reg-end)))))

(provide 'indiumeval-parse-elisp)
;;; indiumeval-parse-elisp.el ends here
