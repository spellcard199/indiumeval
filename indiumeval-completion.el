;;; indiumeval-completion.el --- Get completions from Indium -*- lexical-binding:t -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>

;; SPDX-License-Identifier: GPL-3.0-or-later

;;; Commentary:
;;
;; Completion for javascript inside (indiumeval-eval...) and
;; (indiumeval-call...).
;;

;;; Code:

(require 'indium)
(require 'indiumeval-parse-elisp)

;; (indiumeval-completion--get-prefix :: String -> String)
(defun indiumeval-completion--get-prefix (js-code)
  "Get prefix after last dot (if present) given JS-CODE."
  (if (string-match-p "\\." js-code)
      (car (last (split-string js-code "\\.")))
    js-code))

;; (indiumeval-completion--bounds-of-arg-at :: [String] -> Number -> Region)
(cl-defun indiumeval-completion--bounds-of-arg-at
    (allowed-remote-func-names &optional (starting-point (point)))
  "Return bounds of argument of containing calling function.
ALLOWED-REMOTE-FUNC-NAMES is a list of names that containing calling functions
can have.
STARTING-POINT is a point comprised by the region where the argument is."
  (let* ((remote-func-name-region
          (indiumeval-parse-elisp--calling-func-region
           allowed-remote-func-names)))
    (when remote-func-name-region
      (let ((remote-func-beg (car remote-func-name-region))
            code-text-beg
            code-text-end)
        (save-excursion
          (goto-char remote-func-beg)
          (backward-char)
          (cl-do ((index 0 (1+ index)))
              ((> (point) starting-point)
               ;; 1- and 1+: skip the double quotes
               (setq code-text-end (1- (point)))
               (backward-sexp)
               (setq code-text-beg (1+ (point))))
            (forward-sexp)))
        `(,code-text-beg . ,code-text-end)))))

;; (indiumeval-completion--string-overlapping-ends :: String -> String -> String)
(defun indiumeval-completion--string-overlapping-ends
    (str-with-suffix str-with-prefix)
  "Return overlapping ends of 2 strings.
Example: The following returns \"fr\":
 (indiumeval-completion--string-overlapping-ends \"Array.fr\" \"from\")
Argument STR-WITH-SUFFIX is a string whose suffix can match
STR-WITH-PREFIX's prefix.
Argument STR-WITH-PREFIX is a string whose prefix can match
STR-WITH-SUFFIX's suffix."
  (cl-do ((s str-with-suffix
             (substring s 1 (length s))))
      ((string-prefix-p s str-with-prefix)
       s)))

;; (indiumeval-completion-get :: String -> (String -> [String] -> [String]) -> Mixed)
(defun indiumeval-completion-get (js-code handle-matches-callback)
  "Adapted from `indium-repl-get-completions'.
Get completions for JS-CODE as if typed in the repl.
Argument HANDLE-MATCHES-CALLBACK is a function that takes 2 parameters:
- JS-CODE
- list of strings representing available completions"
  (let* ((prefix (indiumeval-completion--get-prefix js-code))
	 (expression  (if (string-match-p "\\." js-code)
			  (replace-regexp-in-string
                           "\\.[^\\.]*$" "" js-code)
                        "this")))
    (indium-client-get-completion
     expression
     indium-debugger-current-frame
     (lambda (candidates)
       (funcall handle-matches-callback
                js-code
		(seq-filter (lambda (candidate)
                              (string-prefix-p prefix candidate))
                            candidates))))))

;; (indiumeval-completion-get-sync :: String -> [String])
(defun indiumeval-completion-get-sync (js-code)
  "Ask Indium completions for JS-CODE and wait for response."
  (let ((completions :pending))
    (indiumeval-completion-get js-code
                               (lambda (_js-code compls)
                                 (setq completions compls)))
    (while (equal completions :pending)
      (sleep-for 0 10))
    completions))

;; (indiumeval-completion-at-point--callback :: String -> [String] -> String | Buffer -> Mixed)
(defun indiumeval-completion-at-point--callback
    (js-code completions buffer-dest)
  "Read completions.
Argument JS-CODE: code for which completions have been found.
Argument COMPLETIONS: list of strings corresponding to matches.
Argument BUFFER-DEST: buffer where the completions that the user's
choice should be inserted into."
  ;; TODO:
  ;; 1. Maybe learn company instead of using `completing-read' and
  ;; passing `buffer-dest' around.
  ;; 2. Study how `company-indium-repl' works.
  (let* ((prefix (indiumeval-completion--get-prefix js-code))
         (choice (completing-read (concat js-code " : ")
                                  completions
                                  nil nil prefix)))
    (with-current-buffer buffer-dest
      (kill-backward-chars (length prefix))
      (insert choice))))

;; (indiumeval-completion-at-point-func :: Nil -> [Mixed])
(defun indiumeval-completion-at-point-func ()
  "Complete `indiumeval-call...' string args as if inside Indium's JS REPL."
  (let ((bounds-of-js-code (indiumeval-completion--bounds-of-arg-at
                            '("indiumeval-eval"
                              "indiumeval-eval-sync"
                              "indiumeval-call"
                              "indiumeval-call-sync")
                            (point))))
    (when bounds-of-js-code
      (condition-case _err
          (let* ((jscode-beg (car bounds-of-js-code))
                 (jscode-end (cdr bounds-of-js-code))
                 (jscode (buffer-substring-no-properties jscode-beg
                                                         jscode-end))
                 (completions
                  (indiumeval-completion-get-sync jscode)))
            (let* ((reg-beg
                    (- jscode-end
                       (length
                        (indiumeval-completion--string-overlapping-ends
                         jscode
                         (if completions
                             (car completions)
                           jscode-beg))))
                    ;; Replaced so we don't need to check for
                    ;; javascript's dot syntax:
                    ;; (save-excursion
                    ;;    (goto-char jscode-end)
                    ;;    (if (looking-back "\\.[^ ]*" jscode-beg)
                    ;;        (1+ (match-beginning 0))
                    ;;      jscode-beg))
                    )
                   (reg-end jscode-end)
                   (compl-data (list reg-beg
                                     reg-end
                                     completions)))
              ;; (print compl-data)
              compl-data))
        ((error) ;; something happened, do not complete
         ;; (message (prin1-to-string _err))
         (message
          "[indiumeval-call-completion-at-point-function] Error.")
         nil)))))


;; (indiumeval-completion-enable :: Nil -> Nil)
(defun indiumeval-completion-enable ()
  "Enable completion inside args of `indiumeval-call-...'.
Arguments of `indiumeval-call-...' should be strings."
  (interactive)
  (add-to-list 'completion-at-point-functions
               #'indiumeval-completion-at-point-func))

;; (indiumeval-completion-disable :: Nil -> Nil)
(defun indiumeval-completion-disable ()
  "Disable completion inside args of `indiumeval-call-...'."
  (interactive)
  (setq completion-at-point-functions
        (remove #'indiumeval-completion-at-point-func
                completion-at-point-functions))
  nil)


(provide 'indiumeval-completion)

;;; indiumeval-completion.el ends here
