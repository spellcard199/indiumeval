;;; indiumeval-call.el --- Call js functions from elisp via Indium -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;
;; Utility functions to call and pass arguments to JavaScript
;; functions.  `indium-eval' is used under the hood.
;;

(require 'cl-lib)
(require 'indiumeval-eval)

;;; Code:

(defun indiumeval-call--format-js-string-arg (js-string-arg)
  "Workaround for passing strings as args via Indium to JS functions.

Argument JS-STRING-ARG is a string argument to be passed to a
javascript function.

Indium has problems with some characters and newlines that we are
bypassing using base64 with no newlines.

Decoding base64 is done:
- in browsers with: atob('...')
- in Node.js with: new Buffer('...', 'base64').toString()
If atob is not defined we assume Node.js, which has Buffer."
  (cl-check-type js-string-arg string)
  (format
   "(typeof atob !== 'undefined' ? atob : function(a){return new Buffer(a, 'base64').toString()})(%S)"
   (base64-encode-string
    (encode-coding-string js-string-arg 'utf-8)
    t)))

(defun indiumeval-call (func-name sync callback &rest args)
  "Call a js function FUNC-NAME with ARGS using `indium-eval'.

Examples:

  (indiumeval-call \"console.log\" :sync nil 33)

In the 2 following examples the result is the same: in both cases
format produces the same string:
- (format \"%s\" \"33\")
- (format \"%s\" 33)

  (indiumeval-call \"Math.max\" :sync nil \"33\" \"44\")
  (indiumeval-call \"Math.max\" :sync nil 33 44)

If you want to pass a string argument this function supports a
shorthand for using (format \"%S\") explicitly:

  (indiumeval-call \"console.log\" :sync nil '(str \"foobar\"))

Argument SYNC: if non-nil waits for the js function to return and
returns the corresponding `indium-remote-object'.
Argument CALLBACK: elisp function taking an `indium-remote-object'
that is called when the javascript function has returned."
  (cl-check-type func-name string)
  (cl-check-type sync t)
  (cl-check-type callback (or function null))
  (when callback
    (cl-assert (= 1 (length (help-function-arglist callback)))))
  (let ((code
         (concat func-name "("
                 (mapconcat
                  (lambda (arg)
                    (if (and (listp arg) (equal 'str (car arg)))
                        (indiumeval-call--format-js-string-arg (cadr arg))
                      (format "%s" arg)))
                  args ",")
                 ")")))
    (if sync
        (indiumeval-eval-sync code callback)
      (indium-eval code callback))))

(defun indiumeval-call-sync (func-name &rest args)
  "Call `indiumeval-call' which defaults to sync and no callback.

Argument FUNC-NAME: is the name of the function to call.
Argument ARGS: arguments to pass to the js functions.  See
`indiumeval-call' for details about syntax."
  (cl-check-type func-name string)
  (apply #'indiumeval-call `(,func-name :sync nil ,@args)))

(provide 'indiumeval-call)
;;; indiumeval-call.el ends here
