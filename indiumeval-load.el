;;; indiumeval-load.el --- Eval contents of files and dirs in indium via `indium-eval' -*- lexical-binding: t -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;

(require 'indium)

;;; Code:

;; (indiumeval-load-file :: String -> String? -> String? -> Nil)
(defun indiumeval-load-file
    (file &optional pre-code post-code)
  "Send content of `FILE' to Indium's JS REPL.
Argument PRE-CODE is code to be prependend fo file contents before
evaluation.
Argument POST-CODE is code prependend fo file contents before
evaluation."
  (with-temp-buffer
    (when pre-code (insert pre-code))
    (insert-file-contents file)
    (goto-char (point-max))
    (when post-code (insert post-code))
    ;; (indium-eval-buffer) ; this does not suppress output
    (indium-eval (buffer-string) nil)))

;; (indiumeval-load-files :: [String] -> String? -> String? -> Nil)
(defun indiumeval-load-files
    (files-to-load &optional pre-code post-code)
  "Load files in the current indium REPL.
`FILES-TO-LOAD': list of files to evaluate into the current indium
client.
Arguments PRE-CODE and POST-CODE are described in
`indiumeval-load-file'"
  (cl-assert (listp files-to-load))
  (mapc (lambda (file)
          (indiumeval-load-file
           file pre-code post-code))
        files-to-load))

;; (indiumeval-load-dir--directory-files-js :: String -> Nil)
(defun indiumeval-load-dir--directory-files-js (dir)
  "List files ending in .js inside `DIR'."
  (directory-files dir t "[^.]\\.js\\'"))

;; (indiumeval-load-dir :: String -> String? -> String? -> Nil)
(defun indiumeval-load-dir (dir &optional pre-code post-code)
  "Evaluate contents of all .js files in DIR inside Indium's JS REPL.
Arguments PRE-CODE and POST-CODE are described in
`indiumeval-load-file'"
  (indiumeval-load-files
   (indiumeval-load-dir--directory-files-js dir)
   pre-code post-code))

(provide 'indiumeval-load)
;;; indiumeval-load ends here
