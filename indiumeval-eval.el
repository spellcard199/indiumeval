;;; indiumeval-eval.el --- Wrappers around `indium-eval' -*- lexical-binding: t -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;

;;; Code:

(require 'cl-lib)
(require 'indium)

(defun indiumeval-eval-sync (string &optional callback throw-err)
  "Wrapper for `indium-eval'.
Evaluate `STRING' with `indium-eval', wait for evaluation to
complete and return the indium-remote-object when done.
Optional argument CALLBACK is an elisp function that is called when
evaluation is completed.  Takes 1 argument of type
indium-remote-object.
Optional argument THROW-ERR: when non-nil and the js evaluation
resulted in an error, throw an error in elisp also."
  (cl-check-type string string)
  (cl-check-type callback (or function null))
  (let ((pending t)
        (result nil))
    (indium-eval string
                 (lambda (iro)
                   (setq pending nil)
                   (setq result iro)
                   (when callback
                     (funcall callback iro))))
    (while pending
      (sleep-for 0 1))
    (when (and throw-err (indium-remote-object-error-p result))
      (error (prin1-to-string result)))
    result))

(cl-defun indiumeval-eval-loop
    (string
     iro-predicate
     &optional
     (loop-interval-ms 100) ;; millisedonds
     (timeout-sec 60))          ;; seconds
  "Repeatedly evaluate `JS-CODE' synchronously.

Argument STRING is evaluated repeatedly until the result of calling
IRO-PREDICATE becomes true.

Argument IRO-PREDICATE: function that takes 1 argument: an object of
type `indium-remote-object'.

Argument LOOP-INTERVAL-MS: interval between evaluations in
ms.  Rebember that if the resulting representation of the object is
large reducing the interval between evaluation can actually make this
slower rather than faster.

Argument TIMEOUT-SEC: seconds after with give up and return nil."
  (cl-check-type string string)
  (cl-check-type iro-predicate function)
  (cl-check-type loop-interval-ms (or number null))
  (cl-check-type timeout-sec (or number null))
  (cl-assert (= 1 (length (help-function-arglist iro-predicate))))
  (let ((pending t)
        (result nil)
        (start-time (current-time))
        ;; Set defaults if nil
        (timed-out nil))
    (while pending
      (if (> (float-time (time-since start-time))
             timeout-sec)
          (progn
            (setq pending nil) ;; timed-out: stop loop
            (setq timed-out t))
        (setq result (indiumeval-eval-sync string nil t))
        ;; If iro-predicate is satisfied we don't need to wait for
        ;; another evaluation.
        (if (funcall iro-predicate result)
            (setq pending nil) ;; ok: stop loop
          (sleep-for 0 loop-interval-ms))))
    (if timed-out nil result)))

(provide 'indiumeval-eval)
;;; indiumeval-eval ends here
