;;; indiumeval-parse-js.el --- Functions for getting data from js code -*- lexical-binding: t -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;; Functions for parsing and getting data from js code.

;;; Code:

(require 'js2-mode)

;; (indiumeval-parse-js-func-param-names :: String -> [String])
(defun indiumeval-parse-js-func-param-names (js-func-string)
  "Get function parameters from definition using `js2-mode's parser.
JS-FUNC-STRING is the code of the function
E.g. you can pass the result of func_name.toString() as JS_FUNC_STRING
to get parameter names."
  (let* ((function-node (with-temp-buffer
                          (insert js-func-string)
                          (car (js2-ast-root-kids (js2-parse)))))
         (function-node-params (js2-function-node-params function-node))
         (node-names (mapcar #'js2-name-node-name
                             function-node-params)))
    node-names))

(provide 'indiumeval-parse-js)
;;; indiumeval-parse-js.el ends here
