;;; indiumeval-test.el --- Tests for indiumeval.el -*- lexical-binding:t -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>

;; SPDX-License-Identifier: GPL-3.0-or-later

;;; Commentary:
;; Tests for indiumeval.

(require 'buttercup)
(require 'lsp)
(require 'indium)
(require 'indiumeval-eval)
(require 'indiumeval-load)
(require 'indiumeval-call)
(require 'indiumeval-completion)
(require 'indiumeval-call-eldoc)

;;; Code:

(defconst indiumeval-test-dir
  (file-name-directory (or load-file-name
                           (buffer-file-name))))

(defun indiumeval-test--repl-started-p ()
  "Return non-nil if Node.js repl has started.
Works by checking if Node.js repl has printed the welcome message."
  (let ((buf (indium-repl-get-buffer)))
    (when buf
      (with-current-buffer buf
        (string-match-p
         "Welcome to Node.js"
         (buffer-substring-no-properties (point-min) (point-max)))))))

(defun indiumeval-test--launch-sync ()
  "Launch Indium and wait for Node.js repl to print welcome message."
  (let* ((default-directory
           (expand-file-name "js" indiumeval-test-dir))
         (timeout 5)
         (sleep-interval 0.1)
         (counter 0))
    (indium-launch)
    (while (and (not (indiumeval-test--repl-started-p))
                (< counter (/ timeout sleep-interval)))
      (sleep-for sleep-interval)
      (setq counter (1+ counter)))))

(describe

 "indiumeval"

 (before-all
  (setq-local indium-client--process-port
              (lsp--find-available-port "127.0.0.1" 20000))
  (setq-local indium-client-executable
              (expand-file-name "js/node_modules/indium/bin/indium"
                                indiumeval-test-dir))
  (indiumeval-test--launch-sync))

 (before-each nil)

 (after-all
  (indium-quit))

 (it "can launch indium"
     (expect (indiumeval-test--repl-started-p)))

 (it "can eval sync"
     (expect (string-equal
              "true"
              (indium-remote-object-to-string
               (indiumeval-eval-sync "true")))))

 (it "can call sync"
     (expect (string-equal
              "1"
              (indium-remote-object-to-string
               (indiumeval-call-sync "Math.floor" "1.1")))))

 (it "can load dir"
     (expect (let ((dir-to-load (expand-file-name "js/some_files" indiumeval-test-dir)))
               (indiumeval-load-dir dir-to-load)
               (string-equal "3"
                             (indium-remote-object-to-string
                              (indiumeval-eval-sync "a + b;"))))))
 (it "can get completions"
     (expect (equal '("from")
                    (indiumeval-completion-get-sync "Array.fr"))))

 (it "can provide completion-at-point"
     (expect (equal '(30 32 ("from"))
                    (with-temp-buffer
                      (insert "(indiumeval-eval-sync \"Array.fr\")")
                      (backward-char 2)
                      (indiumeval-completion-at-point-func)))))

 (it "can provide eldoc-documentation-function"
     (expect (equal
              (indiumeval-call-eldoc--hightlight--function-argument "foobar"
                                                                    "(\"a\" \"b\")"
                                                                    1)
              (with-temp-buffer
                (indiumeval-eval-sync "function foobar(a, b) {return a + b;}")
                (insert "(indiumeval-call-sync \"foobar\" \"\")")
                (backward-char 2)
                (emacs-lisp-mode)
                (indiumeval-call-eldoc-documentation-function))))))

(provide 'indiumeval-test)
;;; indiumeval-test.el ends here
