;;; indiumeval.el --- Utilities for evaluating Js code from Elisp using Indium -*- lexical-binding: t -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>

;; Author: spellcard199 <spellcard199@protonmail.com>
;; Maintainer: spellcard199 <spellcard199@protonmail.com>
;; Keywords: emacs-lisp, tools, javascript
;; Homepage: https://gitlab.com/spellcard199/indiumeval
;; Package-Requires: ((emacs "26.1") (indium "2.1.4"))
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Version: 0.0.1

;; This file is not part of GNU Emacs.

;;; Commentary:
;;
;; Utilities for evaluating Javascript code from Elisp using Indium.
;;

;;; Code:

(require 'indiumeval-eval)
(require 'indiumeval-call)
(require 'indiumeval-load)
(require 'indiumeval-completion)
(require 'indiumeval-call-eldoc)

(provide 'indiumeval)
;;; indiumeval.el ends here
