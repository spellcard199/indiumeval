#!/bin/env bash
CUR_DIR="$(pwd)"

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd "$SCRIPTS_DIR/.." \
    && printf "Cleaning .elc files... " \
    && find ".cask" -type f -name '*.elc' -exec rm {} + \
    && echo "done." \
    && echo "Running elsa... " \
    && cask exec elsa --load "$SCRIPTS_DIR/elsa-debug-workaround-config.el" "indiumeval.el" \
    && echo "done."

cd "$CUR_DIR"
