#!/bin/env bash

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [[ ! -d "$SCRIPTS_DIR/../.cask" ]]; then
    (cd "$SCRIPTS_DIR/.." && cask install)
fi

if [[ ! -f "$SCRIPTS_DIR/../test/js/node_modules/indium/bin/indium" ]]; then
    (cd "$SCRIPTS_DIR/../test/js" && npm install)
fi

(cd "$SCRIPTS_DIR/.." && cask exec buttercup -L .)
