;;; elsa-debug-workaround-config.el --- configure elsa-debug-workaround -*- lexical-binding:t -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>

;; SPDX-License-Identifier: GPL-3.0-or-later

;;; Commentary:
;; Configure elsa-debug-workaround.

(require 'elsa-debug-workaround)

;;; Code:

(setq elsa-debug-workaround-skip-file-regexps
  '("/company.el"
    "/indium-*[a-z]*.el"
    "/js2-mode.el"))

(elsa-debug-workaround-process-file-advice-add)

(provide 'elsa-debug-workaround-config)
;;; elsa-debug-workaround-config.el ends here
