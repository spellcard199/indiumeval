;;; indiumeval-call-eldoc.el --- Eldoc js in (indiumeval-call...) -*- lexical-binding: t -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;
;; Eldoc inside (indiumeval-call ...) and (indiumeval-call-sync ...).
;;

;;; Code:

(require 'indiumeval-eval)
(require 'indiumeval-parse-elisp)
(require 'indiumeval-parse-js)

;; (indiumeval-call-eldoc-js-func-param-names :: String -> Mixed)
(defun indiumeval-call-eldoc-js-func-param-names (jsfunc-name)
  "Get parameter names for JSFUNC-NAME using live Indium REPL."
  (let* ((js-code (concat jsfunc-name ".toString()"))
         (jsfunc-body (indium-remote-object-to-string
                       (indiumeval-eval-sync js-code)))
         (jsfunc-body-stripped (string-remove-suffix
                                "\""
                                (string-remove-prefix
                                 "\"" jsfunc-body)))
         (jsfunc-is-func (string-prefix-p
                          "function" jsfunc-body-stripped)))
    (if jsfunc-is-func
        (elisp-function-argstring (indiumeval-parse-js-func-param-names
                                   jsfunc-body-stripped))
      :not-a-function)))

;; (indiumeval-call-eldoc--hightlight--function-argument :: String -> String -> Number -> String)
(defun indiumeval-call-eldoc--hightlight--function-argument
    (jsfunc-name jsfunc-param-names arg-index)
  "Highlight ARG-INDEX for JSFUNC-NAME with JSFUNC-PARAM-NAMES."
  (apply #'elisp--highlight-function-argument
         (cond
          ;; Emacs 26
          ((= 4 (length (help-function-arglist
                         'elisp--highlight-function-argument)))
           (list jsfunc-name jsfunc-param-names arg-index ""))
          ;; Emacs 27
          ((= 3 (length (help-function-arglist
                         'elisp--highlight-function-argument)))
           (list jsfunc-name jsfunc-param-names arg-index)))))

;; (indiumeval-call-eldoc-documentation-function :: Nil -> String?)
(defun indiumeval-call-eldoc-documentation-function ()
  "Return eldoc for js functions called by indiumeval-call....
Requires a running Indium's JS REPL where the called function is
defined."
  (let ((func-name-region
         (when (and (equal major-mode 'emacs-lisp-mode)
                    ;; Currently the way `indiumeval-call' works is
                    ;; accepting js arguments in strings: if we are
                    ;; not in a string there is no reason to show
                    ;; eldoc.
                    ;; `in-string-p' is obsolete since 25.1;
                    ;; use (nth 3 (syntax-ppss)) instead.
                    (nth 3 (syntax-ppss)))
           (indiumeval-parse-elisp--calling-func-region
            '("indiumeval-call"
              "indiumeval-call-sync")))))
    (when func-name-region
      (let* ((arg-index
              (indiumeval-parse-elisp--arg-index-at
               (car func-name-region)
               (point)))
             (jsfunc-name (buffer-substring-no-properties
                           (car func-name-region)
                           (cdr func-name-region)))
             (jsfunc-param-names (indiumeval-call-eldoc-js-func-param-names
                                  jsfunc-name)))
        (if (equal jsfunc-param-names :not-a-function)
            (concat "Function " jsfunc-name " does not exist.")
          (indiumeval-call-eldoc--hightlight--function-argument
           jsfunc-name
           jsfunc-param-names
           arg-index))))))

;; (indiumeval-call-eldoc-enable :: Nil -> Nil)
(defun indiumeval-call-eldoc-enable ()
  "Enable eldoc inside args of `indiumeval-call-...'.
Arguments of `indiumeval-call-...' should be strings."
  (interactive)
  (add-function :before-until
                ;; (local 'eldoc-documentation-function)
                (symbol-value 'eldoc-documentation-function)
                #'indiumeval-call-eldoc-documentation-function))

;; (indiumeval-call-eldoc-disable :: Nil -> Nil)
(defun indiumeval-call-eldoc-disable ()
  "Disable completion and eldoc inside args of `indiumeval-call-...'."
  (interactive)
  (remove-function (symbol-value 'eldoc-documentation-function)
                   #'indiumeval-call-eldoc-documentation-function)
  nil)

(provide 'indiumeval-call-eldoc)
;;; indiumeval-call-eldoc ends here
